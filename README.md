# workshiftplanner-vue

## setup
initial setup
* `npm install`

development
before you start the development server, make sure that the workshiftplanner backend is running on localhost:8000, since requests to http://devserver/api/* are proxied to the real backend (see [index.js](config/index.js)).
* `npm run dev`

## build for production

* `npm run build`
to build the files directly into the workshiftplanner backend project you can configure _index_ and _assetRoot_ accordingly in [index.js](config/index.js)
```javascript
  // ...
    index: path.resolve(__dirname, '../../workshiftplanner-php/web/vue/index.html'),
    assetsRoot: path.resolve(__dirname, '../../workshiftplanner-php/web/vue'),
  // ...
```