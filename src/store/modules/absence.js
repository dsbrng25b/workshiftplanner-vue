import * as base from './base'
import AbsenceService from '../../api/AbsenceService'

var api = new AbsenceService()

const state = {
  ...base.state()
}

const getters = {
  ...base.getters
}

const actions = {
  ...base.actions(api)
}

const mutations = {
  ...base.mutations
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
