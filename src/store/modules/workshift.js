import * as base from './base'
import WorkshiftService from '../../api/WorkshiftService'

var api = new WorkshiftService()

const state = {
  ...base.state()
}

const getters = {
  ...base.getters
}

const actions = {
  ...base.actions(api)
}

const mutations = {
  ...base.mutations
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
