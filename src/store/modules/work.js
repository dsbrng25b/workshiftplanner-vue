import * as base from './base'
import WorkService from '../../api/WorkService'

var api = new WorkService()

const state = {
  ...base.state()
}

const getters = {
  ...base.getters
}

const actions = {
  ...base.actions(api)
}

const mutations = {
  ...base.mutations
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
