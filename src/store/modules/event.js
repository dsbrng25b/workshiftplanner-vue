import * as base from './base'
import EventService from '../../api/EventService'

var api = new EventService()

const state = {
  ...base.state()
}

const getters = {
  ...base.getters
}

const actions = {
  ...base.actions(api)
}

const mutations = {
  ...base.mutations
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
