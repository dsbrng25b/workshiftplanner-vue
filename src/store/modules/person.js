import * as base from './base'
import PersonService from '../../api/PersonService'

var api = new PersonService()

const state = {
  ...base.state()
}

const getters = {
  ...base.getters
}

const actions = {
  ...base.actions(api)
}

const mutations = {
  ...base.mutations
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
