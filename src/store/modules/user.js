import api from '../../api/UserService'

const state = {
  username: '',
  isAdmin: false
}

const getters = {
  isAdmin (state) {
    return state.isAdmin
  },
  isLoggedIn (state) {
    return state.username !== ''
  },
  name (state) {
    return state.username
  }
}

const actions = {
  fetch (context) {
    return api.getUser().then((data) => {
      context.commit('setUser', data)
    })
  }
}

const mutations = {
  setUser (state, data) {
    state.username = data.username
    state.isAdmin = data.isAdmin
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
