import Vue from 'vue'
import Vuex from 'vuex'
import person from './modules/person'
import workshift from './modules/workshift'
import event from './modules/event'
import absence from './modules/absence'
import work from './modules/work'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    person,
    workshift,
    absence,
    event,
    work,
    user
  }
})
