import CalendarEntryService from './CalendarEntryService'

export default class EventService extends CalendarEntryService {
  constructor (config = {}) {
    super('/api/event', config)
  }

  toRest (e) {
    return {
      start_time: e.start_time,
      end_time: e.end_time,
      typ: e.typ,
      description: e.description
    }
  }
}
