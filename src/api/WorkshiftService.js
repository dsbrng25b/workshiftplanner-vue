import CalendarEntryService from './CalendarEntryService'

export default class WorkshiftService extends CalendarEntryService {
  constructor (config = {}) {
    super('/api/workshift', config)
  }

  toRest (w) {
    return {
      start_time: w.start_time,
      end_time: w.end_time,
      person: w.person_id,
      work: w.work_id
    }
  }
}
