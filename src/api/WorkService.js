import BaseService from './BaseService'

export default class WorkService extends BaseService {
  constructor (config) {
    super('/api/work', config)
  }

  toRest (w) {
    return {
      name: w.name
    }
  }
}
