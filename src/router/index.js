import Vue from 'vue'
import Router from 'vue-router'
import UbWelcome from '@/components/UbWelcome'
import UbViewPerson from '@/components/UbViewPerson'
import UbViewWorkshift from '@/components/UbViewWorkshift'
import UbViewCalendar from '@/components/UbViewCalendar'
import UbViewWork from '@/components/UbViewWork'
import UbViewAbsence from '@/components/UbViewAbsence'
import UbFormPerson from '@/components/UbFormPerson'
import UbFormWorkshift from '@/components/UbFormWorkshift'
import UbFormWork from '@/components/UbFormWork'
import UbFormAbsence from '@/components/UbFormAbsence'

// import 'bulma/css/bulma.css'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: UbWelcome
    },
    {
      path: '/person',
      name: 'PersonView',
      component: UbViewPerson
    },
    {
      path: '/person/new',
      name: 'PersonNew',
      component: UbFormPerson
    },
    {
      path: '/person/edit/:id',
      name: 'PersonEdit',
      component: UbFormPerson,
      props: true
    },
    {
      path: '/workshift',
      name: 'WorkshiftView',
      component: UbViewWorkshift
    },
    {
      path: '/workshift/new',
      name: 'WorkshiftNew',
      component: UbFormWorkshift
    },
    {
      path: '/workshift/edit/:id',
      name: 'WorkshiftEdit',
      component: UbFormWorkshift,
      props: true
    },
    {
      path: '/work',
      name: 'WorkView',
      component: UbViewWork
    },
    {
      path: '/work/new',
      name: 'WorkNew',
      component: UbFormWork
    },
    {
      path: '/work/edit/:id',
      name: 'WorkEdit',
      component: UbFormWork,
      props: true
    },
    {
      path: '/absence',
      name: 'AbsenceView',
      component: UbViewAbsence
    },
    {
      path: '/absence/new',
      name: 'AbsenceNew',
      component: UbFormAbsence
    },
    {
      path: '/absence/edit/:id',
      name: 'AbsenceEdit',
      component: UbFormAbsence,
      props: true
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: UbViewCalendar
    }
  ],
  linkExactActiveClass: 'is-active'
})
